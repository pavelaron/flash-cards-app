import React, { Component } from 'react';
import {
  StackNavigator,
} from 'react-navigation';

import CardList from './screens/CardList';
import CardEditor from './screens/CardEditor';

const AppNavigator = StackNavigator({
  List: { screen: CardList },
  Editor: { screen: CardEditor }
});

export default class App extends Component {
  render() {
    return (
      <AppNavigator />
    );
  }
}