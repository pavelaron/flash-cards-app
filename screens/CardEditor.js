import React, { Component } from 'react';
import { 
  Alert,
  ActivityIndicator, 
  Text,
  TextInput,
  View
} from 'react-native';
import ActionButton from 'react-native-action-button';
import CardView from 'react-native-cardview';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from '../style/CardEditorStyle';
import renderIf from '../js/renderIf';

export class CardEditor extends Component {

  static navigationOptions = {
    title: 'Card Editor'
  };

  constructor(props) {
    super(props);

    this.card = {
      _id: null,
      question: null,
      answer: null
    };

    if (props.navigation.state.params) {
      this.card = props.navigation.state.params.card;
    }

    this.state = {
      isLoading: false
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <CardView
          cardElevation={5}
          cardMaxElevation={10}
          cornerRadius={8}
          style={styles.cardView}>
          <Text style={styles.inputLabel}>
            Question: 
          </Text>
          <TextInput
            style={styles.textInput}
            onChangeText={ text => this.setQuestion({text}) }
            value={this.card.question}
          />
          <Text style={styles.inputLabel}>
            Answer: 
          </Text>
          <TextInput
            style={styles.textInput}
            onChangeText={ text => this.setAnswer({text}) }
            value={this.card.answer}
          />
        </CardView>
        {renderIf(this.state.isLoading)(
          <View style={styles.indicatorContainer}>
            <ActivityIndicator/>
          </View>
        )}
        {renderIf(!this.state.isLoading)(
          <ActionButton
            buttonColor="rgb(231, 76, 60)"
            onPress={() => { this.validate(); }}
            renderIcon={ active => <Icon name="md-done-all" style={styles.actionButtonIcon} /> }
          />
        )}
      </View>
    )
  }

  setQuestion = (question) => {
    this.card.question = question.text;
  };

  setAnswer = (answer) => {
    this.card.answer = answer.text;
  };

  validate = () => {
    if (!this.card.question) {
      this.showError('Please specify a question');
    } else if (this.card.question.length < 8) {
      this.showError('Question has to be at least 8 characters long');
    } else if (this.card.question.indexOf('?') === -1) {
      this.showError('Question must contain a "?"');
    } else {
      this.sendData();
    }
  };

  showError = (message) => {
    Alert.alert('Error', message);
  };

  sendData = () => {
    this.setState({
      isLoading: true
    });

    let url = 'http://127.0.0.1:3000/cards';
    if (this.card._id) {
      url += `/${this.card._id}`;
    }

    const encodedQuestion = encodeURIComponent(this.card.question);
    const encodedAnswer = encodeURIComponent(this.card.answer || '');

    const errorMessage = 'Please try again';

    fetch(url, {
      method: this.card._id ? 'PATCH' : 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: `question=${encodedQuestion}&answer=${encodedAnswer}`
    })
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        isLoading: false
      });

      if (responseJson.status === 200) {
        this.props.navigation.goBack();
      } else {
        this.showError(errorMessage);
      }
    })
    .catch((error) => {
      this.setState({
        isLoading: false
      });

      this.showError(errorMessage);
    });
  };
}

export default CardEditor;
