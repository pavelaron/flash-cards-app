import React, { Component } from 'react';
import { 
  Alert,
  FlatList,
  TouchableHighlight,
  ActivityIndicator, 
  Text, 
  View
} from 'react-native';
import ActionButton from 'react-native-action-button';
import CardView from 'react-native-cardview';
import Icon from 'react-native-vector-icons/Ionicons';
import Button from 'react-native-button';
import _ from 'lodash';
import styles from '../style/CardListStyle';

const labelReveal = '(Tap to reveal)';

export class CardList extends Component {

  static navigationOptions = {
    title: 'Cards List'
  };

  constructor(props) {
    super(props);
    
    this.state = { isLoading: true }
    this.props.navigation.addListener('willFocus', payload => {
        this.refresh();
      }
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.centeredOnScreen}>
          <ActivityIndicator/>
        </View>
      )
    } else if (this.state.offline) {
      return (
        <View style={styles.centeredOnScreen}>
          <Text>
            Connection error&#8230;
          </Text>
          <Button
            containerStyle={styles.offlineButtonContainer}
            style={styles.cardButton}
            onPress={() => this.refresh()}
          >
            Reload
          </Button>
        </View>
      )
    } else if (_.isEmpty(this.state.dataSource)) {
      return this.renderWithActionButton(
        <View style={styles.centeredOnScreen}>
          <Text>No cards added yet&#8230;</Text>
        </View>
      )
    }

    return this.renderWithActionButton(
      <FlatList
        contentContainerStyle={styles.listBottomPadding}
        data={this.state.dataSource}
        renderItem={({item}) => this._renderRow(item)}
        keyExtractor={(item) => item._id}
      />
    )
  }

  refresh = () => {
    this.setState({
      isLoading: true
    });

    this.loadCards();
  };

  loadCards = () => {
    return fetch('http://127.0.0.1:3000/cards')
      .then((response) => response.json())
      .then((responseJson) => {
        this.hideRandomly(responseJson.response);
        this.setState({
          isLoading: false,
          offline: false,
          dataSource: responseJson.response,
        });
      })
      .catch((error) => {
        this.setState({
          isLoading: false,
          offline: true
        });
      });
  };

  renderWithActionButton = (content) => {
    return (
      <View>
        { content }
        <ActionButton
          buttonColor="rgb(231, 76, 60)"
          onPress={this._addPressed}
        />
      </View>
    )
  };

  hideRandomly = (cards) => {
    _.map(cards, (card) => {
      card.questionHidden = _.random(1);
      return card;
    });
  };

  _renderRow = (item) => {
    return (
      <TouchableHighlight 
        style={{ padding: 20 }}
        activeOpacity={0.6}
        underlayColor='transparent'
        onPress={() => { 
          this._onPressRow(item)
        }
      }>
        <CardView
          cardElevation={5}
          cardMaxElevation={10}
          cornerRadius={8}
          style={styles.cardView}>
          <Text style={styles.cardQuestion}>
            Question: { item.questionHidden ? labelReveal : item.question }
          </Text>
          <Text style={styles.cardAnswer}>
            Answer: { item.questionHidden ? item.answer : labelReveal }
          </Text>
          <Button
            containerStyle={styles.cardButtonContainer}
            style={styles.cardButton}
            onPress={() => this._editPressed(item)}
          >
            <Icon name="md-create" style={styles.cardButton} />
          </Button>
        </CardView>
      </TouchableHighlight>
    )
  };

  _onPressRow = (rowData) => {
    const title = rowData.questionHidden ? 'Question' : 'Answer';
    const message = rowData.questionHidden ? rowData.question : rowData.answer;

    Alert.alert(title, message);
  };

  _editPressed = (selectedCard) => {
    this.props.navigation.navigate('Editor', { card: selectedCard });
  };

  _addPressed = () => {
    this.props.navigation.navigate('Editor');
  };
}

export default CardList;
