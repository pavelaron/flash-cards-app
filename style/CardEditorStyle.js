'use strict';

const ReactNative = require('react-native');
const styles = ReactNative.StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  cardView: {
    width: '100%', 
    height: 200,
    backgroundColor: 'white',
    borderStyle: 'solid',
    borderColor: '#eee',
    borderWidth: 1,
    padding: 15
  },
  actionButtonIcon: {
    color: 'white', 
    fontSize: 16
  },
  inputLabel: {
    marginTop: 15
  },
  textInput: {
    height: 40, 
    borderColor: 'gray', 
    borderWidth: 1,
    padding: 5
  },
  indicatorContainer: { 
    marginTop: 40,
    flex: 1
  }
});

export default styles;
