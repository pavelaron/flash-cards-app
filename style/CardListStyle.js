'use strict';

const ReactNative = require('react-native');
const styles = ReactNative.StyleSheet.create({
  centeredOnScreen: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  offlineButtonContainer: {
    padding: 10,
    width: 120,
    height: 40,
    marginTop: 20,
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 20, 
    backgroundColor: 'gray'
  },
  listBottomPadding: {
    paddingBottom: 100
  },
  cardView: {
    width: '100%', 
    height: 'auto',
    backgroundColor: 'white',
    borderStyle: 'solid',
    borderColor: '#eee',
    borderWidth: 1,
    padding: 30
  },
  cardQuestion: {
    fontSize: 20, 
    color: 'black', 
    textAlign: 'center', 
  },
  cardAnswer: {
    marginTop: 15,
    marginBottom: 30,
    textAlign: 'center'
  },
  cardButtonContainer: {
    padding: 10,
    width: 40, 
    height: 40, 
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 20, 
    backgroundColor: 'gray'
  },
  cardButton: {
    fontSize: 18, 
    color: 'white',
    marginLeft: 3
  }
});

export default styles;
